@extends('layouts.app')
@section('content')
<h1>Lista de preguntas</h1>

<a class="btn btn-secondary" href="/questions/create">Nuevo</a>

<table class="table table-bordered">
    <tr>
        <th>pregunta</th>
        <th>a</th>
        <th>b</th>
        <th>c</th>
        <th>d</th>
        <th>respuesta</th>
        <th>modulo</th>
    </tr>

    @foreach ($questions as $question)
    <tr>
        <td>{{ $question->text }}</td>
        @if ($question->answer == 'a')
            <td class="bg-success">
        @else
            <td>
        @endif
        {{ $question->a }}</td>


        @if ($question->answer == 'b')
            <td class="bg-success">
        @else
            <td>
        @endif
            {{ $question->b }}</td>

        @if ($question->answer == 'c')
            <td class="bg-success">
        @else
            <td>
        @endif
            {{ $question->c }}</td>

        @if ($question->answer == 'd')
            <td class="bg-success">
        @else
            <td>
        @endif
            {{ $question->d }}</td>

        <td>{{ $question->answer }}</td>
        <td>{{ $question->module->name }}</td>

    </tr>
    @endforeach
</table>

{{ $questions->links() }}

<hr>

@endsection
