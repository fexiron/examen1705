@extends('layouts.app')
@section('content')
<h1>Alta de modulo</h1>

<form method="post" action="/questions">
    {{ csrf_field() }}

    <div class="form-group">
    <label>text</label>
    <input type="text" name="text" class="form-control" value="{{ old('text') }}">
    </div>

    <div class="form-group">
    <label>a</label>
    <input type="text" name="a" class="form-control" value="{{ old('a') }}">
    </div>

    <div class="form-group">
    <label>b</label>
    <input type="text" name="b" class="form-control" value="{{ old('b') }}">
    </div>

    <div class="form-group">
    <label>c</label>
    <input type="text" name="c" class="form-control" value="{{ old('c') }}">
    </div>

    <div class="form-group">
    <label>d</label>
    <input type="text" name="d" class="form-control" value="{{ old('d') }}">
    </div>

    <div class="form-group">
    <label>answer</label>
    <input type="text" name="answer" class="form-control" value="{{ old('d') }}">
    </div>

    <div class="form-group">
        <label>module</label>
        <select class="form-control" name="module_id">
            @foreach ($modules as $module)
                <option value="{{ $module->id }}">{{ $module->name }}</option>
            @endforeach
        </select>
    </div>

    <div class="alert-danger">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </div>

     <div class="form-group">
    <input type="submit" name="" value="Crear" class="form-control">
    </div>
</form>

@endsection
